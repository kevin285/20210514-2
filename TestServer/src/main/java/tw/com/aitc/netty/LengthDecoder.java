package tw.com.aitc.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteOrder;

public class LengthDecoder extends LengthFieldBasedFrameDecoder implements CodesLengthFormat {
	private static final Logger LOG = LoggerFactory.getLogger(LengthDecoder.class);
	private byte type = TEXT_FORMAT;
	private String leader ="";

	public LengthDecoder (String type, int maxFrameLength, int lengthFieldOffset, int lengthFieldLength,  
            int lengthAdjustment, int initialBytesToStrip, String leader) {
		super(maxFrameLength, lengthFieldOffset, lengthFieldLength,
				lengthAdjustment, initialBytesToStrip);
		switch (type) {
			case "T":
				this.type=TEXT_FORMAT;
				break;
			case "B":
				this.type=HEX_BE_FORMAT;
				break;
			case "b":
				this.type=HEX_LE_FORMAT;
				break;
			case "P":
				this.type=BCD_FORMAT;
				break;
		}
		this.leader=leader;
	}
	
	@Override 
	protected long getUnadjustedFrameLength(ByteBuf buf, int offset, int length, ByteOrder order) {
		long frameLength = 0;
		byte[] baLen = null;
		ByteBuf tmp = null;
		
		if(checkLeader(buf)) {
		switch (type) {
			case HEX_BE_FORMAT:
				frameLength = super.getUnadjustedFrameLength(buf, offset, length, ByteOrder.BIG_ENDIAN);
				break;
			case HEX_LE_FORMAT:
				frameLength = super.getUnadjustedFrameLength(buf, offset, length, ByteOrder.LITTLE_ENDIAN);
				break;
			case BCD_FORMAT:
				baLen = new byte[length];
				
				tmp = buf.getBytes(offset, baLen, 0, baLen.length);
				for (int i = 0; i < baLen.length; i++) {
					int i0 = baLen[i] / 16;
					int i1 = baLen[i] % 16;

					frameLength = frameLength * 100 + i0 * 10 + i1;
				}
				break;
			case TEXT_FORMAT:
			default:
				baLen = new byte[length];
				
				tmp = buf.getBytes(offset, baLen, 0, baLen.length);
				
				LOG.debug("getUnadjustedFrameLength(): tmp->[{}] baLen->[{}]", tmp, new String(baLen));
				frameLength = Integer.parseInt(new String(baLen));
				break;
		}

		LOG.debug("getUnadjustedFrameLength() {}, frameLength:[{}]", buf.refCnt(), frameLength);
		
        return frameLength;
		}
		return -1;
	}
	
	protected boolean checkLeader(ByteBuf buf) {
		byte[] Letter = null;
		ByteBuf tmp = null;
		Letter = new byte[leader.getBytes().length];
		tmp = buf.getBytes(0, Letter, 0, Letter.length);
		String leaderLetter = new String(Letter);
		LOG.debug("checkLeader(): tmp->[{}], leaderLetter->[{}]", tmp, leaderLetter);
		if(leader.equals("") || leader.equals(leaderLetter)) {
			return true;
		}
		return false;
	}
}
