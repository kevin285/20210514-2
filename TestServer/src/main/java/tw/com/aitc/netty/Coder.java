package tw.com.aitc.netty;

import io.netty.channel.ChannelHandler;
import org.apache.camel.component.netty.ChannelHandlerFactory;
import org.apache.camel.component.netty.DefaultChannelHandlerFactory;

import java.nio.ByteOrder;

public class Coder {
	private Coder() {
    }

    public static ChannelHandlerFactory newLengthFieldBasedFrameDecoder(final String type, final int maxFrameLength, final int lengthFieldOffset,
                                                                        final int lengthFieldLength, final int lengthAdjustment,
                                                                        final int initialBytesToStrip,final String leader) {
		return new DefaultChannelHandlerFactory() {
			@Override
			public ChannelHandler newChannelHandler() {
				return new LengthDecoder(type, maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip, leader);
			}
		};
    }

    public static ChannelHandlerFactory newLengthFieldPrepender(final boolean type, final ByteOrder byteOrder, final int lengthFieldLength, final int lengthAdjustment,
			final boolean lengthIncludesLengthFieldLength, final String leader) {
				return new DefaultChannelHandlerFactory() {

					@Override
					public ChannelHandler newChannelHandler() {
						return new LengthPrepender(type,
					            byteOrder,lengthFieldLength,
					            lengthAdjustment, lengthIncludesLengthFieldLength,leader);
					}};
				}
}
