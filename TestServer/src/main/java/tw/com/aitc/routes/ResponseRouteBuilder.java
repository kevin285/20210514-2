package tw.com.aitc.routes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.channel.ChannelHandler;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.spi.PropertiesComponent;
import org.apache.camel.support.DefaultRegistry;
import tw.com.aitc.funtions.SimpleFuntion;
import tw.com.aitc.netty.Coder;
import tw.com.aitc.netty.DecoderByteArrayDecoder;
import tw.com.aitc.netty.EncoderByteArrayEncoder;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class ResponseRouteBuilder extends RouteBuilder {
    public void configure() throws Exception {
        List<ChannelHandler> encoders = new ArrayList<>();
        encoders.add(Coder.newLengthFieldPrepender(false, ByteOrder.BIG_ENDIAN, 2, 0, false, ""));
        encoders.add(new EncoderByteArrayEncoder());

        List<ChannelHandler> decoders = new ArrayList<>();
        decoders.add(Coder.newLengthFieldBasedFrameDecoder("B", Integer.MAX_VALUE, 0, 2, 0, 2, ""));
        decoders.add(new DecoderByteArrayDecoder());

        DefaultRegistry registry = this.getContext().getRegistry(DefaultRegistry.class);

        registry.bind("Encoder", encoders);
        registry.bind("Decoder", decoders);

        registry.bind("httpCounter", new AtomicInteger(0));
        registry.bind("tcpCounter", new AtomicInteger(0));

        // Rest API 設定
        restConfiguration()
                .component("undertow")
                .host("0.0.0.0")
                .port("{{Http_Receive_Port}}")
                .bindingMode(RestBindingMode.off);

        // Rest API 的 URI 設置
        rest("Rex")
                .post("/jmeter/test")
                .toD("direct:TestHttp");

        // 處理流程
        from("direct:TestHttp")
                .id("TestHttp")
                .log(LoggingLevel.ERROR, "Get Message!")
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        PropertiesComponent prop = exchange.getContext().getPropertiesComponent();
                        Boolean errSet = Boolean.parseBoolean(prop.parseUri("Http_Err"));
                        Integer errTimingSet;
                        String body = exchange.getIn().getBody(String.class);
                        log.error("Body:" + body);
                        // 從 registry 取出 httpCounter 變數
                        DefaultRegistry registry = exchange.getContext().getRegistry(DefaultRegistry.class);
                        AtomicInteger count = registry.lookupByNameAndType("httpCounter", AtomicInteger.class);

                        // 計數器變數數值+1
                        int i = count.incrementAndGet();

                        log.error("httpCounter:" + i);

                        // 若錯誤設定在system.properties中有開啟
                        if (errSet) {
                            errTimingSet = Integer.parseInt(prop.parseUri("Http_ErrTiming"));
                            // 若計數器目前數值為errTimingSet的倍數，則丟出exception
                            if (i % errTimingSet == 0) {
                                throw new Exception("Http Test Error");
                            }
                        }

                        // 取出Json格式電文，並放入response欄位，再放回exchange中
                        ObjectMapper objectMapper = new ObjectMapper();
                        JsonNode reqBody = objectMapper.readTree(body);
                        Map<String, String> rspBody = objectMapper.convertValue(reqBody, new TypeReference<Map<String, String>>() {
                        });
                        if (exchange.getIn().getHeader("Encryption").toString().equalsIgnoreCase("true")) {
                            SimpleFuntion simpleFuntion = new SimpleFuntion();
                            String msg = simpleFuntion.hexStringToString(rspBody.get("Content"));
                            log.error("Decode Body:{\"Content\":\"" + msg + "\"}");
                            if (msg.equals("Test")) {
                                rspBody.put("Response", simpleFuntion.stringToHexString("Succ"));
                            } else {
                                rspBody.put("Response", simpleFuntion.stringToHexString("Fail"));
                            }
                        } else {
                            if (rspBody.get("Content").equals("Test")) {
                                rspBody.put("Response", "Succ");
                            } else {
                                rspBody.put("Response", "Fail");
                            }
                        }
                        String rspMessage = objectMapper.writeValueAsString(rspBody);
                        exchange.getMessage().setBody(rspMessage);
                    }
                });

        from("netty:tcp://localhost:{{Tcp_Receive_Port1}}?textline=true&sync=true")
                .setProperty("TcpType", simple("1"))
                .to("direct:TestTcp");

        from("netty:tcp://localhost:{{Tcp_Receive_Port2}}?decoders=#Decoder&encoders=#Encoder&sync=true")
                .setProperty("TcpType", simple("3"))
                .to("direct:TestTcp");

        from("direct:TestTcp")
                .id("TestTcp")
                .log(LoggingLevel.ERROR, "Get Message!")
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        // 從exchange抓取system.properties資料
                        PropertiesComponent prop = exchange.getContext().getPropertiesComponent();
                        Boolean errSet = Boolean.parseBoolean(prop.parseUri("Tcp_Err"));
                        Integer errTimingSet;
                        String msgBody = exchange.getIn().getBody(String.class);

                        log.error("Body:" + msgBody);

                        // 從 registry 取出 tcpCounter 變數
                        DefaultRegistry registry = exchange.getContext().getRegistry(DefaultRegistry.class);
                        AtomicInteger count = registry.lookupByNameAndType("tcpCounter", AtomicInteger.class);

                        // 計數器變數數值+1
                        int i = count.incrementAndGet();

                        log.error("tcpCounter:" + i);

                        // 若錯誤設定在system.properties中有開啟
                        if (errSet) {
                            errTimingSet = Integer.parseInt(prop.parseUri("Tcp_ErrTiming"));
                            // 若計數器目前數值為errTimingSet的倍數，則丟出exception
                            if (i % errTimingSet == 0) {
                                if (exchange.getProperty("TcpType", String.class).equalsIgnoreCase("3")) {
                                    log.error("TcpType:" + 3);
                                    String rspMessage = "java.lang.Exception: Tcp Test Error";
                                    exchange.getMessage().setBody(convert2hex(rspMessage));
                                    return;
                                } else {
                                    throw new Exception("Tcp Test Error");
                                }

                            }
                        }
                        // 設定回應Thanks，再放回exchange中
                        if (exchange.getProperty("TcpType", String.class).equalsIgnoreCase("3")) {
                            log.error("TcpType:" + 3);
                            String rspMessage;
                            if (msgBody.equalsIgnoreCase("Test")) {
                                rspMessage = "Succ.";
                            } else {
                                rspMessage = "Fail.";
                            }
                            exchange.getMessage().setBody(convert2hex(rspMessage));
                        } else {
                            // 換行符用作結尾符，JMeter tcp請求中的End of line(EOL) byte value 需設為 10
                            String rspMessage;
                            if (msgBody.equalsIgnoreCase("Test")) {
                                rspMessage = "Succ.\r";
                            } else {
                                rspMessage = "Fail.\r";
                            }
                            exchange.getMessage().setBody(rspMessage);
                        }

                    }
                });

        log.error("Complete preparation");
    }

    private byte[] convert2hex(String message) {
        String str = message;

        char[] chars = "0123456789abcdef".toCharArray();
        StringBuilder sb = new StringBuilder("");
        byte[] bs = str.getBytes();
        int bit;
        for (int i = 0; i < bs.length; i++) {
            bit = (bs[i] & 0x0f0) >> 4;
            sb.append(chars[bit]);
            bit = bs[i] & 0x0f;
            sb.append(chars[bit]);
        }

        byte[] val = new byte[sb.toString().trim().length() / 2];
        for (int i = 0; i < val.length; i++) {
            int index = i * 2;
            int j = Integer.parseInt(sb.toString().trim().substring(index, index + 2), 16);
            val[i] = (byte) j;
        }
        return val;
    }
}
